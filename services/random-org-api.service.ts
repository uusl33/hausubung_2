import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PostBody } from 'types/random-org/post-body';

@Injectable({
  providedIn: 'root',
})
export class RandomOrgApiService {
  constructor(private http: HttpClient) {}

  async getNumbers(request: PostBody) {
    const url = 'https://api.random.org/json-rpc/2/invoke';
    const params = {
      apiKey: '78fc3cad-e628-4105-beab-ce7098c7052c',
      ...request,
    };
    const body = {
      jsonrpc: '2.0',
      method: 'generateIntegers',
      params: params,
      id: 42,
    };
    return await this.http.post(url, body);
  }
}
