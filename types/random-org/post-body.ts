export interface PostBody {
  n: number;
  min: number;
  max: number;
  replacement?: boolean;
}
