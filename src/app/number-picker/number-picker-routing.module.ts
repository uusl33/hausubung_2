import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NumberPickerComponent } from './number-picker.component';

const routes: Routes = [{ path: '', component: NumberPickerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NumberPickerRoutingModule { }
