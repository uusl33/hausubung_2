import { Component } from '@angular/core';
import { RandomOrgApiService } from 'services/random-org-api.service';
import { PostBody } from 'types/random-org/post-body';

@Component({
  selector: 'app-number-picker',
  templateUrl: './number-picker.component.html',
  styleUrls: ['./number-picker.component.scss'],
})
export class NumberPickerComponent {
  constructor(private randomOrgApiService: RandomOrgApiService) {}
  startNumber = 0;
  endNumber = 1024;

  min = -2147483648;
  max = 2147483647;

  result?: number = undefined;

  ngOnInit() {}

  onStartChange(number: string) {
    this.startNumber = parseInt(number);
    if (this.startNumber >= this.endNumber) {
      this.endNumber = this.startNumber + 1;
    }
  }

  onEndChange(number: string) {
    this.endNumber = parseInt(number);
    if (this.endNumber <= this.startNumber) {
      this.startNumber = this.endNumber - 1;
    }
  }

  async getRandomNumber() {
    if (this.endNumber === undefined) {
      alert('Please select an end date.');
      return;
    }
    const request: PostBody = {
      n: 1,
      min: this.startNumber,
      max: this.endNumber,
      replacement: false,
    };
    (await this.randomOrgApiService.getNumbers(request)).subscribe(
      (res: any) => {
        console.log(res);
        this.result = res.result.random.data[0];
      }
    );
  }
}
