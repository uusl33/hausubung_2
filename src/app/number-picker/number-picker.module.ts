import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NumberPickerRoutingModule } from './number-picker-routing.module';
import { NumberPickerComponent } from './number-picker.component';
import { NbButtonModule, NbCardModule, NbInputModule } from '@nebular/theme';

@NgModule({
  declarations: [NumberPickerComponent],
  imports: [
    CommonModule,
    NumberPickerRoutingModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
  ],
})
export class NumberPickerModule {}
