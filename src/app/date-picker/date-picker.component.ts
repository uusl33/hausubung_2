import { Component } from '@angular/core';
import { NbCalendarRange } from '@nebular/theme';
import { RandomOrgApiService } from 'services/random-org-api.service';
import { PostBody } from 'types/random-org/post-body';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
})
export class DatePickerComponent {
  constructor(private randomOrgApiService: RandomOrgApiService) {
    this.DateRange = {
      start: this.startDate,
      end: this.endDate,
    };
  }
  startDate = new Date();
  endDate? = new Date(this.startDate.getTime() + 86400000 * 30);

  minDate = new Date(0);
  maxDate = new Date(2147472000000);

  DateRange: NbCalendarRange<Date>;
  rangeChange: any;

  resultDate?: Date = undefined;

  ngOnInit() {}

  onRangeChange(event: NbCalendarRange<Date>) {
    this.startDate = event.start;
    this.endDate = event.end;
  }

  async getRandomDate() {
    if (this.endDate === undefined) {
      alert('Please select an end date.');
      return;
    }
    const request: PostBody = {
      n: 1,
      min: Math.floor(this.startDate.getTime() / 86400000),
      max: Math.floor(this.endDate.getTime() / 86400000),
      replacement: false,
    };
    (await this.randomOrgApiService.getNumbers(request)).subscribe(
      (res: any) => {
        console.log(res);
        this.resultDate = new Date(res.result.random.data[0] * 86400000);
      }
    );
  }
}
