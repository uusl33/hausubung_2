import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'date-picker',
    loadChildren: () =>
      import('./date-picker/date-picker.module').then(
        (m) => m.DatePickerModule
      ),
  },
  {
    path: 'number-picker',
    loadChildren: () =>
      import('./number-picker/number-picker.module').then(
        (m) => m.NumberPickerModule
      ),
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
