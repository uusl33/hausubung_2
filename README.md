# Project Name

This is a simple Angular project which includes two main features:

1. Random Date Picker
2. Random Number Picker

![Home Page](./readme/home-page.png)

The images used in this project are sourced from Unsplash.

This project leverages the Nebular UI library to enhance the user interface experience.

## Features

### Random Date Picker

The Random Date Picker page allows the user to select a range of dates from a built-in calendar tool. After selecting a range, by clicking the `Get Random Date` button, the application will generate a random date within the selected range and display it to the user.

![Random Date Picker](./readme/date-picker1.png)
![Random Date Picker](./readme/date-picker2.png)

### Random Number Picker

The Random Number Picker page provides two input boxes labeled 'Starting Number' and 'Ending Number', pre-filled with default values of 0 and 1024, respectively. Upon clicking the `Get Random Number` button, the application will generate a random number within the range specified and display it to the user.

![Random Number Picker](./readme/number-picker.png)

## Project Setup

Follow these steps to set up the project on your local machine:

1. Clone the repository: `git clone https://git.thm.de/uusl33/hausubung_2`
2. Navigate to the project directory: `cd hausubung_2`
3. Install the dependencies: `npm install`
4. Start the server: `npx nx run Hausubung_2:serve`

The application will be running at `http://localhost:4200`.

## Usage

### Random Date Picker

1. Navigate to the Random Date Picker page from the Home page.
2. Click on the input box to open the calendar.
3. Select a range of dates.
4. Click on the `Get Random Date` button.
5. The application will display a randomly picked date within the selected range.

### Random Number Picker

1. Navigate to the Random Number Picker page from the Home page.
2. The input boxes 'Starting Number' and 'Ending Number' will have default values of 0 and 1024, respectively.
3. Adjust these numbers if you wish to set a different range.
4. Click the `Get Random Number` button.
5. The application will display a random number within your selected range.

## Contact

If you have any questions, suggestions, or want to discuss things further, feel free to open an issue or reach out directly.

---

This README.md file is generated with the help of ChatGPT, a language model by OpenAI.
